import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Navigation from './src/navigation/Navigation';
import SplashScreen from 'react-native-splash-screen'

export default class App extends Component {
  componentDidMount() {
    setTimeout(() => {
      SplashScreen.hide();
    }, 3000);
  }

  render() {
    return (
      <NavigationContainer>
        <Navigation />
      </NavigationContainer>
    )
  }
}