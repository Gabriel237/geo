import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { title, textBtn, } = this.props;
    return (
        <View style = {styles.footer}>
          <View>
            <Image source = {require('@assets/imgs/footer.png')} 
                   style = {styles.footerimg}
                   resizeMode = 'stretch'
            />
          </View>
          <View style = {{flexDirection: 'row',width: 300}}>
            <Text style = {styles.textfooter}>{title}</Text>
            <TouchableOpacity style = {styles.btncreate} 
                              onPress = {() => this.props.navigation.navigate('SignIn')}>
              <Text>{textBtn}</Text>
            </TouchableOpacity>
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    footerimg: {
        height: 100,
        width: 150,
        alignSelf: 'flex-end',
        left: -40,
      },
      footer: {
        position: 'absolute',
        bottom: 1,
        flexDirection: 'row',
        alignItems: 'center'
      },
      textfooter: {
        right: 50,
        color: '#777'
      },
      btncreate: {
        right: 50,
        borderBottomColor: '#f4bc19',
        borderBottomWidth: 1
      }
});

export default Footer;
