import React, { Component } from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import { Icon } from 'react-native-elements'

import { colors } from '../styles/index'

class FormGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { label, iconName } = this.props;

    return (
        <View style = {styles.form_group}>
            <View>
                <Icon 
                    type = 'ionicon'
                    name = {iconName}
                    iconStyle = {styles.icon}
                />
            </View>
            <View style = {{flex: 1}}>
                <TextInput placeholder = {label}
                            style = {styles.textInput}/>
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    form_group: {
        flexDirection: 'row',
        borderBottomColor: colors.PRIMARY,
        borderBottomWidth: 1,
        marginBottom: 10,
        alignItems: 'center'
      },
      icon: {
        marginRight: 15,
        color: colors.PRIMARY
      },
      textInput: {
        width: '100%',
      },
});

export default FormGroup;
