import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, StatusBar} from 'react-native';


class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { title } = this.props;

    return (
        <View>
            <View style = {styles.navbar}>
                <View>
                    <ImageBackground source = {require('@assets/imgs/header1.png')} 
                                    style = {styles.headerimg}
                                    resizeMode = 'contain'
                    >
                    <View>
                        <Text style = {styles.titleheader}>{title}</Text>
                    </View>
                    </ImageBackground>
                </View>
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  navbar: {
    position: 'absolute',
    top: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  titleheader: {
    fontSize: 30,
    left: 100,
    top: 100,
    fontWeight: 'bold',
    color: '#fff'
  },
  headerimg: {
    height: 300,
    width: Dimensions.get('window').width - 40,
    top: -40,
    left: -70
  },
});

export default Header;
