import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SignIn from '@SignIn';
import SignUp from '@SignUp';
import SplashScreen from '../utils/splashScreen'

const Stack = createStackNavigator();

function Navigation() {
  return (
      <Stack.Navigator>
        <Stack.Screen name="SplashScreen" component={SplashScreen} options = {{headerShown: false}} />
        <Stack.Screen name="SignUp" component={SignUp} options = {{headerShown: false}} />
        <Stack.Screen name="SignIn" component={SignIn} options = {{headerShown: false}} />
      </Stack.Navigator>
  );
}

export default Navigation;