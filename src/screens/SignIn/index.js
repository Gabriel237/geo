import React, { Component } from 'react';
import { View, Text, StyleSheet, 
         TouchableHighlight,} from 'react-native';

import Header from '../../components/Header';
import FormGroup from '../../components/FormGroup';
import Footer from '../../components/Footer';
import { defaultStyle } from '../../utils/defaultStyle';

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style = {styles.container}>
        <Header title = 'Inscription' />
        <View style = {styles.content}>
          <View style = {styles.card}>
            <FormGroup label = 'Email' iconName = 'md-person' />
            <FormGroup label = 'Télépphone' iconName = 'md-call' />
            <FormGroup label = 'Mot de passe' iconName = 'ios-key' />
          </View>
          <View>
            <TouchableHighlight style = {styles.btnConnect} 
                                onPress = {() => {}}
            >
              <View>
                <Text style = {styles.textbtn}>Inscription</Text>
              </View>
            </TouchableHighlight>
          </View>
        </View>
        <Footer />
      </View>
    );
  }
}

const styles = StyleSheet.create(defaultStyle);

export default SignIn;
