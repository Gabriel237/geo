import React, { Component } from 'react';
import { View, Text, StyleSheet,TouchableHighlight,BackHandler } from 'react-native';

import Header from '../../components/Header';
import FormGroup from '../../components/FormGroup';
import Footer from '../../components/Footer';
import { defaultStyle } from '../../utils/defaultStyle';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', () => {
        BackHandler.exitApp()
      })
  }

  render() {
    return (
      <View style = {styles.container}>
        <Header title = 'Connexion' />
        <View style = {styles.content}>
          <View style = {styles.card}>
            <FormGroup label = 'Email' iconName = 'md-person' />
            <FormGroup label = 'Mot de passe' iconName = 'ios-key' />
          </View>
          <View>
            <TouchableHighlight style = {styles.btnConnect} 
                                onPress = {() => {}}
            >
              <View>
                <Text style = {styles.textbtn}>Connexion</Text>
              </View>
            </TouchableHighlight>
          </View>
        </View>
        <Footer {...this.props} 
                title = "vous n'avez pas de compte?"
                textBtn = 'Créer'
        />
      </View>
    );
  }
}

const styles = StyleSheet.create(defaultStyle);

export default SignUp;
