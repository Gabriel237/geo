import { Dimensions } from 'react-native';

export const defaultStyle = {
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center'
      },
      content: {
        flex: 1,
        justifyContent: 'center'
      },
      card: {
        width: Dimensions.get('window').width - 40,
        marginLeft: 20,
        marginRight: 20,
        borderRadius: 7,
        padding: 15,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {height: 2, width: 2},
        shadowOpacity: 0.1,
        shadowRadius: 15,
        elevation: 15,
      },
      btnConnect: {
        width: 150,
        height: 45,
        top: 30,
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        backgroundColor: '#f4bc19',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-end'
      },
      textbtn: {
          color: '#fff',
          fontSize: 20
      },
}

