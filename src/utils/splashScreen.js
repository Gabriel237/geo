import React, { Component } from 'react';
import { View, Text, ImageBackground, StyleSheet, Image } from 'react-native';
import * as Animatable from 'react-native-animatable';

class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
      setTimeout(() => {
          this.props.navigation.navigate('SignUp')
      }, 7000);
  }

  render() {
    return (
      <View style = {styles.container}>
        <ImageBackground source = {require('@assets/imgs/bgsplash.png')} style = {styles.bg}>
            <Animatable.Image animation="pulse" easing="ease-out" iterationCount="infinite" source = {require('@assets/imgs/logo.png')} style = {styles.logo} ></Animatable.Image>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    bg: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        width: 100,
        height: 100
    }
});

export default SplashScreen;
